Thre are tons of organizations out there!

# Sections
[DEF CON year round](#def-con-year---round)
[Identity based communities](#identity-based-year-round-communities)
[DEF CON Communities](#def-con-communities)
[Communities not based on identity](#communities-not-based-on-identity)
[DEF CON Villages](#def-con-villages)
[DEF CON Events](#def-con-events)
[DEF CON Contests](#def-con-contests)

# Content

## DEF CON year - round

### DEF CON Groups
Missing DEF CON? Coming to DEF CON for the first time and want to make like minded friends? These local (worldwide) groups keep the DEF CON feeling going all year round. Check out their website to find your local group. If there is not a group local to you - you only need two volunteers to start organizing one.
https://defcongroups.org

### Lonely Hackers Club
Afraid of doing DEF CON alone? No one you know near you into hacking? The LHC is a loose collective of people interested in hacking. LHC began on /r/defcon as a way for people attending DEF CON ‘alone’ to meet up with other people.
https://lonelyhackers.club/

### 2600
If you read 2600 - there are local 2600 meetups which have a high overlap of hackers, perhaps check this out if the above options dont' work for you.

## Identity based communities

### The Diana Initiative
https://www.dianainitiative.org/ all underrepresented groups, allies welcome. No membership required, mostly focused on an annual event and Hacker Summer Camp.

### BBWIC Foundation
https://www.bbwic.com/ To cultivate a future where women in cybersecurity globally lead with confidence, collaborate seamlessly, and inspire lasting impact in the ever-evolving industry. Empowering Women in Cybersecurity Globally for Leadership and Growth.
#sparkleleadership Membership based organization.

### Black Girls Hack
https://blackgirlshack.org/ not limited to women or BIPOC, everyone welcome. They are a membership based organization.

### Blacks In Technology Foundation
https://blacksintechnology.org/ The largest community of Black people in the technology industry. Through community-focused activities, events and outreach, The Blacks In Technology (BIT) Foundation is “Stomping the Divide” by establishing a blueprint of world-class technical excellence and innovation by providing resources, guidance, networking, and opportunities for members to share their expertise and advance their careers.

### Latinas in Cyber
https://www.latinasincyber.com/

### La Villa
 We want every Latino, regardless of speaking Spanish or Portuguese, to feel that he is at home within Defcon, that he can express his thoughts, ideas, research, work, that we can unite on the same topic and talk about what we are passionate about. …hacking and security.
https://lavillahacker.com/
 
 ### Raices Cyber
 The Hispanic, Latino, Latina, LatinX and Our Allies International Association in Cybersecurity." Our Mission is to Encourage and Support the Hispanic and Latino Cyber and Technology Community to Achieve Greater Representation in the World. We are doing that with our four main pillars. Continued Support and Encouragement to and from the Community, Education for all career levels, Access to valuable resources, Constructive Networking and the forming of STRONG bonds (“Roots”).
https://www.raicescyber.org/
 
### Women in Security and Privacy
Women in Security and Privacy (WISP) is an organization dedicated to advancing women and underrepresented communities in the fields of privacy and security. Their mission is to empower individuals by providing education, mentorship, networking opportunities, and support for career growth in security and privacy domains .WISP’s commitment to diversity and inclusion makes it a vital force in shaping a more equitable and secure digital landscape.
https://www.wisporg.com/

### Women in Cybersecurity
Women in CyberSecurity (WiCyS) is a 501©(3) non-profit organization dedicated to supporting the recruitment, retention, and advancement of women in the field of cybersecurity. WiCyS provides a vibrant community for women and their supporters, offering various resources and opportunities. Members can benefit from professional development programs, mentorship opportunities, and virtual/in-person conferences and career fairs. Additionally, WiCyS maintains a job board and provides valuable resources for women in cybersecurity. The organization actively promotes diversity and inclusion in the cybersecurity workforce, connecting members through the WiCyS Member Community Portal and special interest groups. 
https://www.wicys.org/

### Women’s Society of Cyberjutsu (WSC)
The Women’s Society of Cyberjutsu (WSC) is a 501©3 non-profit organization founded in 2012 with a mission to empower women and girls in cybersecurity careers. Their focus is on raising awareness about cybersecurity career opportunities, advancing women in the field, and closing the gender gap in information security roles. WSC achieves this through networking, education, training, mentoring, and other professional opportunities. Their community provides a supportive environment for women to fearlessly enter, advance, and lead in the world of cybersecurity. Membership based organization with an annual event and local chapters!
https://womenscyberjutsu.org/

### Sober in Cyber
Sober in Cyber is a nonprofit organization with a clear mission: to create alcohol-free events and foster community-building opportunities for individuals in the cybersecurity industry who choose to live a sober lifestyle.

As a volunteer-led initiative, we are committed to organizing both in-person and virtual gatherings. These events provide a welcoming space for those abstaining from alcohol for various reasons—whether they are sober, sober-curious, participating in Dry January, managing health-related concerns, or undergoing athletic training. Attendees can engage in professional networking with fellow infosec peers without any pressure to drink. Our events include NA (non-alcoholic) options, mocktails, and other sober-friendly activities within the cybersecurity community. Here’s to a supportive and alcohol-free environment! 
https://www.soberincyber.org/

### DC Furs
DEFCON Furs is a program of HACK YOUR LIVES, a California based 501(c)(3) non-profit organization (83-1213517). Hack Your Lives is a community supported organization that strives to promote, support, and advance the idea that everyone should be free to hack their lives in a safe and supportive environment. We are focused on organizing and providing support for individuals and groups that promote creating and owning a life and identity that is yours.
https://dcfurs.com/

### QueerCon
QueerCon is an annual student-organized LGBTQIA2S+ art, comics, society, and media fan convention and conference held at Western Washington University in Bellingham, WA. Celebrating its 8th year, QueerCon provides a vibrant space for queer creators and intersectional representation in comics, arts, society, and media. The event features panelists, talks, performances, games, and film screenings, fostering community and celebrating diversity.
https://queercon.org/

### VetCon

## DEF CON Communities

## Communities not based on identity

### FIRST
FIRST (Forum of Incident Response and Security Teams) is a global organization that brings together incident response teams from various sectors to enhance incident prevention, promote information sharing, and improve overall security. With over 700 members worldwide, FIRST collaborates through technical colloquia, hands-on classes, and an annual incident response conference. 
https://www.first.org/

### OWASP Foundation
OWASP is a nonprofit organization dedicated to improving software security. It provides free and open source tools, standards, and documentation for securing web applications. With a focus on web application security, OWASP offers valuable resources, hosts global conferences, and maintains the well-known OWASP Top Ten list of critical security risks. Developers and security professionals benefit from OWASP’s community-driven efforts and commitment to enhancing application protection against cyber threats
https://owasp.org/

### Whole Cyber Human Initiative
https://www.wholecyberhumaninitiative.org/
The Whole Cyber Human Initiative is a forward-thinking organization that places human well-being at the heart of cybersecurity. Their mission extends beyond technical skills, emphasizing the importance of mental health, work-life balance, and community support. Through workshops, resources, and advocacy, they empower cybersecurity professionals to thrive holistically, fostering a healthier and more resilient industry.

## DEF CON Villages

### Adversary Village
https://adversaryvillage.org

### Aerospace Village

### Blue Team Village

### Car Hacking Village

### Crypto Privacy Village

### Hardware Hacking/Soldering Skills Village
### ICS Village
The ICS Village equips industry professionals and policymakers to defend industrial equipment. Through experiential awareness, education, and training, it addresses critical infrastructure security. Interactive simulated ICS environments provide hands-on experiences with real components, enhancing preparedness against changing threats. Explore more at the ICS Village website
https://www.icsvillage.com/

### IOT Village
### Lock Pick Village
### Radio Frequency Village

## DEF CON Events

### Toxic BBQ
https://www.toxicbbq.org/p/zine.html

## DEF CON Contests

### DEF CON MUD
The DEFCON MUD is a virtual world that is remade every year for various conferences. A MUD stands for Multi User Dungeon, and is a Text Based Game. Be prepared to enter into a virtual text based game. It occurs before DEF CON itself and is a great fun way to lead into Hacker Summer Camp.

### Hacker Runway
Hack3r Runw@y brings out all the chic geeks out there. It encourages rethinking fashion in the eyes of hackers/pentesters. Be it smartwear, LED additions, obfuscation, cosplay or just everyday wear using fabrics and textures that are familiar to the community. Contestants can enter clothing, shoes, jewelry, hats or accessories. If it can be worn, it is perfect for the runway. For convenience, contestants can enter the contest with designs made ahead of the conference, however it needs to be made by them and not just store bought. Hack3r Runway is perfect for everyone whether technologically savvy or just crafty.
https://hack3rrunway.github.io/