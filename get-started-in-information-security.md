## I Want to get started in information security

Great!

However you are going to need to do a lit more research on your own first before approaching people with questions.

Why?

It's a HUGE field with many specializations, no one can impart all that data to you, also part of being in information security is being able to (and possibly enjoying) research and learning.

### Get Started By Reading

#### Free

-https://tisiphone.net/2015/10/12/starting-an-infosec-career-the-megamix-chapters-1-3/
-https://github.com/tkisason/getting-started-in-infosec
-https://gist.github.com/mubix/5737a066c8845d25721ec4bf3139fd31?permalink_comment_id=3701964

### Paid (books)
-https://www.manning.com/books/cybersecurity-career-guide
-https://www.keirstenbrager.tech/securetheinfosecbag/

### Next why don't you try out

#### Podcasts
- Federal Service specific - https://podcasts.apple.com/us/podcast/national-security-commission-on-ai/id1515619487

#### Recorded Information Security talks

Search youtube

#### An information Security conference

Find your local BSides

#### Capture The Flag (CTF)

There are many online and free!

##### Federal Service
- Start your cybersecurity career with the U.S. government, Scholarship For Service (SFS) https://sfs.opm.gov/

#### Other Info Dumps

- https://github.com/NicoleSchwartz/Learning-to-code-resources/wiki/Secure-Coding-or-Information-Security

##### Certifications

- federal service - consider Sec+ or a CISSP (not a starter)