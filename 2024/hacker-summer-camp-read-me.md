## Resources for 2024 Hacker Summer Camp

### What is Hacker Summer Camp?
It's the time period each summer when, usually in the before times, many Information Security / Cyber Security / Hacker Conferences occur in Las Vegas, Nevada, USA. As a result many people spend the whole week in Las Vegas in the summer learning (and often partying) which has earned it the nickname "hacker summer camp".

The main events:

- [Black Hat USA](https://www.blackhat.com/) August 3-8, 2024
- [Diana Initiative](https://www.dianainitiative.org/) Mon Aug 5, 2024
    - [Tickets](https://www.eventbrite.com/e/the-diana-initiative-2024-tickets-773825511937?aff=website)
- [BSidesLV](https://www.bsideslv.org/) Tue Aug 6-7, 2024 
    - [Skytalks](https://skytalks.info/) at BSidesLV Tue Aug 6-7, 2024
- [Girls Hack Village presents SquadCon](https://www.blackgirlshack.org/event-5495307?CalendarViewType=1&SelectedDate=8/30/2024) August 7-8, 2024
- [Unofficial DEF CON Shoot](https://deviating.net/firearms/defcon_shoot/) August 7, 2024
- [DEF CON](https://defcon.org/) Aug 8-11, 2024 
  - [DEF CON Villages](https://hackervillages.org/)
- [Shabbatcon](https://shabbatcon.com/#food) 8/11-8/12
- [Queercon](https://www.queercon.org/) - Meetups / Mixers TBD
- [DC Furs](https://dcfurs.com/) - TBD

### What is Birds of a Feather

We are trying to gather a list of as many resources, events and programs as possible occuring at summer camp and provide links to them to help you create the best time for you. We will try and tag the events with who the event or resource is aimed at, who is welcome, who is discouraged, so you can flock together with other birds like you.

### Wondering about DEF CON hotels?

- [How Close Will Your Hotel be to DEF CON?](https://www.youtube.com/watch?v=m9TTmME3a8w)
- [A Small DEF CON Hotel Distance Update](https://www.youtube.com/watch?v=z_l9LM3Siuw)

### Def Con Compilations

- [HackerVillage Website](https://hackervillages.org/)
- [Outel consolidated schedules](http://defcon.outel.org/)
- [all the twitches](https://multitwitch.tv/defconorg/defcon_dctv_one/defcon_dctv_four/defcon_music/defcon_chill/aivillage/biohackingvillage/blueteamvillage/bypassvillage/cryptovillage/dcpolicy/hackthesea/dchhv/hamradiovillage/ics_village/iotvillage/monerovillage/passwordvillage/paymentvillage/redteamvillage/roguesvillage/toool_us/votingvillagedc)
- [Reddit master post](https://www.reddit.com/r/Defcon/comments/tx7tg2/mega_def_con_info_for_your_planning_enjoyment/)

### Chats / Discussion areas

- "Hack Girls Summer" https://twitter.com/coriplusplus/status/1395216621246025729 https://twitter.com/coriplusplus/status/1395216621246025729
- https://twitter.com/kjvalentine/status/1151901023704739845
- [hackercon slack](https://join.slack.com/t/hackercon/shared_invite/enQtMjk0NTc1MjgzNjY1LWE3M2ExY2MwZGQ4ODdkYWRmZDU2YTc4OWZhY2Y0NDMxZTQ0MzVkMDYwYTI4ODdiNjZlNDQzNjhjMzI0M2Q2YzU), one Chanel per con, trying to keep the number of slacks lower!
- [we are hackerz slack](https://www.womenhackerz.com/)
- [veteran sec](https://veteransec.com/)
- [L0nelyH4ckers](https://twitter.com/L0nelyH4ckers) - resources for solo travelers - with a telegram chat that's active year-round

### Social Media

- https://twitter.com/CircuitSwan/lists/hacker-summer-camp - this is a list of accounts!
- https://twitter.com/womenofdefcon - Women focused, non-binary welcome
- https://twitter.com/queercon - they will be having non-binary focused events with their partner
- https://twitter.com/defconparties - http://defconparties.com/ 
- https://twitter.com/_beyondbinaries
- https://twitter.com/wisporg
- https://twitter.com/WomenCyberjutsu

#### *DC Darknet*

- https://dcdark.net/home

### Specific Events

#### *GothCon* TBD

- Details: https://twitter.com/dcgothcon
- For: Hackers, goths, and all friendly adjacents welcome.

### Lonely Hackers Club

http://lonelyhackers.club/

### Useful Applications

- https://hackertracker.app/ 

### Ham Radio / Amateur Radio

- https://github.com/travisgoodspeed?tab=repositories 
- http://conham.org/

### Conference guides / tips-and-tricks

- TDI's Tips & Tricks document https://docs.google.com/document/d/1blR6SYN21anGQQtQgVUU428LPFyvjTxnl0ZxCQpsFLQ/edit#heading=h.7g7otz81689t
- TDI's Food Document https://docs.google.com/document/d/1EJmN5O-EzaD9hNU-AkHRfL818t995denQgAasxi7K0E/edit
- TDI's June Livestream https://www.youtube.com/watch?v=X_lH0P0zhjI&t=4s
- TDI's July Livestream https://www.youtube.com/watch?v=rka2NfmkTyA
- Cheap Food Deals by DEF CON music https://defconmusic.org/def-con-31-resources/cheap-vegas-food-deals/
- DEF Con first timer advice https://www.youtube.com/watch?v=ezo0Lf-7l6M
- deviantollam https://www.youtube.com/watch?v=AsPeB6bc5ho
- https://sites.google.com/site/amazonv/first-conference?authuser=0
  - specific for under 18 https://sites.google.com/site/amazonv/first-conference?authuser=0#h.om0pifo0kg9w
  - specific for under 21 https://sites.google.com/site/amazonv/first-conference?authuser=0#h.jym9ujy0368
- https://lostpolicymaker.org/planningyourtrip/
- https://www.markloveless.net/blog/2019/7/24/las-vegas-summer-camp-survival-guide
- https://github.com/Shad0w-Synd1cate/Hacker-Summer-Camp-Survival-Guide/
- [@Clevrcat’s 2018 InfoSec Con Guide](https://docs.google.com/document/d/14YrFlis9rj9DodQ_vTqTtkSKvUtJh22br__VE5jjcQU/edit)
- [DEF CON: The Ultimate Guide for First-Timers](https://ginnyfahs.medium.com/def-con-the-ultimate-guide-for-first-timers-516b6ffda705)
- https://github.com/largehadroncollider/defcon-guide/blob/master/DEF%20CON%20Guide.md
- http://lonelyhackers.club/post/defconguide/

### Other Resource compliations (like this)

- @defconparties / Google Calender - https://twitter.com/defconparties / https://t.co/eCKZ901ZIJ
- https://conferenceparties.com/hsc2023/
- The One! - On the go single page Hacker Summer Camp calender (good for viewing mobile) - http://defcon.outel.org/
- HACKER DOUBLE SUMMER 2022 GUIDES https://defcon201.medium.com/hacker-double-summer-2022-guides-part-two-capture-the-flags-8321fa224598
- Kosher food https://shabbatcon.com/#food

### Generic Las Vegas information
- Surviving 24 Hours in Las Vegas With Only $50  https://www.youtube.com/watch?v=CnONp2PSkIw
- The 10 Best FREE Things To Do in Las Vegas for 2022! https://www.youtube.com/watch?v=c4_NE2MS_iY
- https://www.tiktok.com/@vegasstarfish

## TBD

### Women's Meetups

- Details: https://defcon.org/html/defcon-32/dc-32-pmne.html
- For: Women, gender non-conforming and non-binary meetup with The Diana Initiative Meetup at DEF CON
- Saturday 19:00-21:00 | Location: 305-306

#### *DEFCON Dinner* TBD

#### *Annual Cyberjutsu Awards* TBD

#### *Hacker Swan*

- Details: http://hackerswan.com
- For: everyone

#### *Hacker Foodies*

- Details: http://hackerfoodies.com
- For: everyone