## Resources for 2020 Hacker Summer Camp

### What is Hacker Summer Camp?
It's the time period each summer when, usually in the before times, many Information Security / Cyber Security / Hacker Conferences occur in Las Vegas, Nevada, USA. As a result many people spend the whole week in Las Vegas in the summer learning (and often partying) which has earned it the nickname "hacker summer camp".

This year it has gone virtual, which expands it so many more people can attend! If the cost of travel or vias is usually prohibitive - this is your chance! We'll deeply miss meeting in person, but hopefully next year.

The main events:

- [HOPE - Hackers On Planet Earth](https://hope.net/) - July 25-Aug 2, 2020
- [h@cktivitycon is a HackerOne hosted hacker conference](https://www.hackerone.com/hacktivitycon)
- [Black Hat USA](https://www.blackhat.com/) - August 1-6, 2020 - four days of technical Trainings (August 1-4) followed by the two-day main conference (August 5-6) featuring Briefings, Arsenal, Business Hall, and more.
- [DEF CON](https://defcon.org/) - August 6-9, 2020 
  - [DEF CON Villages](https://hackervillages.org/)
- [DC Furs](https://2020.dcfurs.com/) August 15, 2020
- [Diana Initiative](https://www.dianainitiative.org/) - August 21-22, 2020 - open to all, women focused

### What is Birds of a Feather

We are trying to gather a list of as many resources, events and programs as possible occuring at summer camp and provide links to them to help you create the best time for you. We will try and tag the events with who the event or resource is aimed at, who is welcome, who is discouraged, so you can flock together with other birds like you.

### Def Con Compilations

- [all the twitches](https://multitwitch.tv/defconorg/defcon_dctv_one/defcon_dctv_four/defcon_music/defcon_chill/aivillage/biohackingvillage/blueteamvillage/bypassvillage/cryptovillage/dcpolicy/hackthesea/dchhv/hamradiovillage/ics_village/iotvillage/monerovillage/passwordvillage/paymentvillage/redteamvillage/roguesvillage/toool_us/votingvillagedc)
-[HackerVillage Website](https://hackervillages.org/)
-[Outel consolidated schedules](http://defcon.outel.org/)

### Chats / Discussion areas

- DEFCON-squad - AKA (Women and Non-Binary) WAN-Party - [fill this out[(https://docs.google.com/forms/d/e/1FAIpQLSd58xof9SF6lgiR_fQYcJpDUZI3e9VD5wFMHk32uatGNexCEg/viewform)
- https://twitter.com/kjvalentine/status/1151901023704739845
- [hackercon slack](https://join.slack.com/t/hackercon/shared_invite/enQtMjk0NTc1MjgzNjY1LWE3M2ExY2MwZGQ4ODdkYWRmZDU2YTc4OWZhY2Y0NDMxZTQ0MzVkMDYwYTI4ODdiNjZlNDQzNjhjMzI0M2Q2YzU), one Chanel per con, trying to keep the number of slacks lower!
- [we are hackerz slack](https://www.womenhackerz.com/)
- [veteran sec](https://veteransec.com/)
- [L0nelyH4ckers](https://twitter.com/L0nelyH4ckers) - resources for solo travelers - with a telegram chat that's active year-round

### Social Media

- https://twitter.com/CircuitSwan/lists/hacker-summer-camp - this is a list of accounts!
- https://twitter.com/womenofdefcon - Women focused, non-binary welcome
- https://twitter.com/queercon - they will be having non-binary focused events with their partner
- https://twitter.com/defconparties - http://defconparties.com/ 
- https://twitter.com/_beyondbinaries
- https://twitter.com/wisporg
- https://twitter.com/WomenCyberjutsu

### Scholarships

### Ongoing / Multi-part Events

#### *Hacker Swan*

- Details: http://hackerswan.com
- For: everyone

#### *Hacker Foodies*

- Details: http://hackerfoodies.com
- For: everyone

#### *DC Darknet*

- https://dcdark.net/home

### Specific Events

### *DEF CON Shoot* TBD

- https://deviating.net/firearms/defcon_shoot/
- Date TBD
- For: everyone welcome

#### *DEF CON PROM* TBD

#### *GothCon* TBD

- Details: https://twitter.com/dcgothcon
- For: Hackers, goths, and all friendly adjacents welcome.

#### *DEFCON Dinner* TBD

#### *Annual Cyberjutsu Awards* TBD

### Swag

- (Stickers)[https://rachelvelas.co/shop/]
- (Shirts)[https://wan-party.myshopify.com/collections/t-shirts] Thanks pigeon!

### Useful Applications

- https://hackertracker.app/ 

### Conference guides / tips-and-tricks

- https://sites.google.com/site/amazonv/first-conference?authuser=0
- https://www.markloveless.net/blog/2019/7/24/las-vegas-summer-camp-survival-guide
- https://github.com/Shad0w-Synd1cate/Hacker-Summer-Camp-Survival-Guide/
- [@Clevrcat’s 2018 InfoSec Con Guide](https://docs.google.com/document/d/14YrFlis9rj9DodQ_vTqTtkSKvUtJh22br__VE5jjcQU/edit)

### Ham Radio / Amateur Radio

- https://github.com/travisgoodspeed?tab=repositories 
- http://conham.org/

### Other Resource compliations (like this)

- @defconparties / Google Calender - https://twitter.com/defconparties / https://t.co/eCKZ901ZIJ
- The One! - On the go single page Hacker Summer Camp calender (good for viewing mobile) - http://defcon.outel.org/

