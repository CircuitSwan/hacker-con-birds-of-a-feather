## Resources for 2019 Hacker Summer Camp

### What is Hacker Summer Camp?
It's the time period when many Information Security / Cyber Security / Hacker Conferences occur in Las Vegas, Nevada, USA

- https://www.blackhat.com/ - August 3-8
- https://bsideslv.org - August 6-7
- https://defcon.org/ - August 8-11
- https://www.queercon.org/ - open to all, and partnering with https://twitter.com/_beyondbinaries 
- https://www.dianainitiative.org/ - open to all, women focused - August 9-10
- https://narwhal.be/ - August 5-11

### Def Con helpline

- [#DEFCON27’s Support Hotline - Attendees can reach #DEFCON staff from 8am to 4am to anonymously report  behavior violating our code of conduct or for an empathic ear - call or text +1 (725) 222-0934. Trained community volunteers will be standing by to help.](https://twitter.com/defcon/status/1154161047865122816)

### What is Birds of a Feather

We are trying to gather a list of as many resources, events and programs as possible occuring at summer camp and provide links to them to help you create the best time for you. We will try and tag the events with who the event or resource is aimed at, who is welcome, who is discouraged, so you can flock together with other birds like you.

### Chats / Discussion areas

- DEFCON27-squad - Women focused, non-binary welcome Discord ask @CircuitSwan
- https://twitter.com/kjvalentine/status/1151901023704739845
- [hackercon slack](https://join.slack.com/t/hackercon/shared_invite/enQtMjk0NTc1MjgzNjY1LWJlN2M1YmFjN2QzM2U4MzNlNTdiNTUyYmU4ZmU2Njc2YWVjY2E4OWIyZWMzZjQwYjRjYjUyMTg0YmQ0NzNiZTY), one Chanel per con, trying to keep the number of slacks lower!

### Social Media

- https://twitter.com/CircuitSwan/lists/hacker-summer-camp - this is a list of accounts!
- https://twitter.com/womenofdefcon - Women focused, non-binary welcome
- https://twitter.com/queercon - they will be having non-binary focused events with their partner
- https://twitter.com/defconparties - http://defconparties.com/ 
- https://twitter.com/_beyondbinaries
- https://twitter.com/wisporg
- https://twitter.com/WomenCyberjutsu

### Ongoing Events

#### *Buddy Program*

- Details:: [Buddy Program Read Me](https://gitlab.com/CircuitSwan/hacker-con-birds-of-a-feather/blob/master/hacker-summer-camp-blackhat-bsideslv-defcon-dianainitiative/2019/buddy-system.md)
- For: Newbies
- To sign up contact @CircuitSwan and get on the DEFCON27-squad discord

#### *Hacker Swan*

- Details: http://hackerswan.com
- For: everyone

#### *Hacker Foodies*

- Details: http://hackerfoodies.com
- For: everyone

#### *DC Darknet*

- https://dcdark.net/home

### Specific Events

#### *DEF CON Meetup* Thursday, August 8

- Details: https://docs.google.com/document/d/1uw2Xhg89qR3RCHc4s1YhP59uKXXu-QZzOyhuK7TMZmI/edit
- Thursday, August 8, 2019 5-7pm in Planet Hollywood’s Sin City Theatre
- For: For the women and non-binary folkx

### *DEF CON Shoot* Wednesday, August 7, 2019

- https://deviating.net/firearms/defcon_shoot/
- Wednesday, August 7, 2019
- For: everyone welcome

#### *Brunch* Sunday, August 11

- Details: RSVP https://www.meetup.com/Hacker-Foodies/events/262693310/?isFirstPublish=true
- Caesars Hotel Forum Food Court, 10am Sunday, August 11, 2019
- For: the women and non-binary folkx

#### *GothCon* Saturday, Aug 10th

- Details: https://twitter.com/dcgothcon
- #DCGOTHCON #defcon official party Sat Aug 10th 10p-2a Gallery in PH 🖤 
- For: Hackers, goths, and all friendly adjacents welcome.

#### *DEFCON Dinner* Friday, August 9th

- https://twitter.com/DEFCONDinner
- "Ok we have a go! Friday, August 9th, 2019 from 6:30 pm until 8:30 pm at the Park by @TMobile Arena. Plenty of dining & beverage choices. Come make friends, eat, imbibe & network before your Friday evening party schedule!"
- For: everyone

#### *6th Annual Cyberjutsu Awards* Thursday, August 8

- https://womenscyberjutsu.org/page/AwardsVegas2019
- August 8, 2019 |  6 - 8 pm | Las Vegas, NV
- The Women's Society of Cyberjutsu (WSC) presents the 6th Annual Cyberjutsu Awards, celebrating #hiddenfigures leading change in cyber and their exemplary contributions to the field. These women serve as role models, mentors, advocates, educators and more to the next generation of cyber talent. Women are nominated by their peers at various stages in their career from Rising Star to Cyber Advocate and all those in between. 

#### *Spa Day*

- Details: TDI Slack (The Diana Initiative)
- For: Women focused, non-binary welcome

#### *Photoshoot*

- Details: TBD
- For: Women focused, non-binary welcome

### Swag

- (Stickers)[https://rachelvelas.co/shop/]
- (Shirts)[https://wan-party.myshopify.com/collections/t-shirts] Thanks pigeon!

### Useful Applications

- https://hackertracker.app/ 

### Conference guides / tips-and-tricks

- https://sites.google.com/site/amazonv/first-conference?authuser=0
- https://www.markloveless.net/blog/2019/7/24/las-vegas-summer-camp-survival-guide
- https://github.com/Shad0w-Synd1cate/Hacker-Summer-Camp-Survival-Guide/

### Ham Radio / Amateur Radio

- https://github.com/travisgoodspeed?tab=repositories 
- http://conham.org/

### Other Resource compliations (like this)

- @defconparties / Google Calender - https://twitter.com/defconparties / https://t.co/eCKZ901ZIJ
- The One! - On the go single page Hacker Summer Camp calender (good for viewing mobile) - http://defcon.outel.org/

